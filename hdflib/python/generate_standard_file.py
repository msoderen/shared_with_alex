import h5py
import numpy as np
import time
from datetime import datetime

#Generate data
t = np.arange(0.0, 4096.0, 1.0)
s = 8192.0*np.sin(0.31 * np.pi * t)
data=np.matrix([s for i in range(3564)],dtype='int16')
data=np.swapaxes(data,0,1)

#generate filename
filename=datetime.now().strftime("07382_Inj_B1H_Q7_%Y%m%d_%Hh%Mm%Ss%fus.h5")


#Create file
f = h5py.File(filename, 'w')
grp = f.create_group("B1")
dset = grp.create_dataset("horizontal", (4096,3564), dtype='int16', chunks=(4096, 1),scaleoffset=0,data=data)
dset.attrs.create("SourceID",data=286327041,shape=(1,),dtype='uint32')
dset.attrs.create("Trigger",data=0,shape=(1,),dtype='uint32')
dset.attrs.create("FreezeTime",data=int(time.time())*1000000,shape=(1,),dtype='uint64')
dset.attrs.create("StartTurn",data=0,shape=(1,),dtype='uint32')
dset.attrs.create("EndTurn",data=4095,shape=(1,),dtype='uint32')
dset.attrs.create("TagBits",data=0,shape=(1,),dtype='uint32')
dset.attrs.create("CRC32Errors",data=0,shape=(1,),dtype='uint32')
f.close()
