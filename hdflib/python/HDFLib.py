# -*- coding: utf-8 -*-
#Author Martin Soderen
import os
import h5py
import numpy as np

rootpath="/nfs/cs-ccr-adtobsnfs/lhc_adtobsbox_data"

class ADTFile:
  def __init__(self,filename):
    self.open=False
    self.filename=filename
    self.SourceID=0
    self.Trigger=0
    self.FreezeTime=0
    self.StartTurn=0
    self.EndTurn=0
    self.TagBits=0
    self.CRC32Errors=0
    #sanity checks
    if not os.path.isfile(self.filename):
      raise RuntimeError('file {} does not exists'.format(self.filename))
    
    self.file=h5py.File(self.filename,'r')

    #extract all datasets
    self.dataset=None
    for group in self.file.keys():
      for dataset in self.file[group].keys():
        data=np.transpose(self.file[group][dataset])
        if data.shape[0]>0 and data.shape[1]>0:
          self.dataset=data
          break
        attrs=self.file[group][dataset].attrs
        if "SourceID" in attrs.keys():
          self.SourceID=attrs["SourceID"][0]
        if "Trigger" in attrs.keys():
          self.Trigger=attrs["Trigger"][0]
        if "FreezeTime" in attrs.keys():
          self.FreezeTime=attrs["FreezeTime"][0]
        if "StartTurn" in attrs.keys():
          self.StartTurn=attrs["StartTurn"][0]
        if "EndTurn" in attrs.keys():
          self.EndTurn=attrs["EndTurn"][0]
        if "TagBits" in attrs.keys():
          self.SourceID=attrs["TagBits"][0]
        if "CRC32Errors" in attrs.keys():
          self.CRC32Errors=attrs["CRC32Errors"][0]

    if type(self.dataset)==None:
      raise RuntimeError('file {} does not contain any 2d datasets'.format(self.filename))
    self.open=True

  def __enter__(self):
    return self
    
  def __exit__(self, type, value, traceback):
    self.close()

  def __getitem__(self,index):
    return self.dataset[index]

  def close(self):
    self.file.flush()
    self.file.close()
