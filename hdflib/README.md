# HDFLib

C++ and python library to abstract away the reading of the HDF files stored following the standard file hierarchy as defined in https://wikis.cern.ch/display/BERF/File+standard

# Python
The data is transposed so accessing index 0 will access the data for bunch 0
```
import HDFLib
f=HDFLib.ADTFile("/home/msoderen/mytestfile.h5")
#the bbb data for bunch 0
f[0]
array([    0,  6775,  7616, ...,  4170, -3487, -8091], dtype=int16)
f[0][1]
6775
```

```
import HDFLib
with plotlib.ADTFile("/home/msoderen/mytestfile.h5") as f
	#the bbb data for bunch 0
	print(f[0])
	print(f.SourceID)
	print(f.Trigger)
	print(f.FreezeTime)
	print(f.StartTurn)
	print(f.EndTurn)
	print(f.TagBits)
	print(f.CRC32Errors)
```

# C++
The data is transposed so accessing index 0 will access the data for bunch 0
```
#include "HDFLib.h"
using namespace HDFLib;

int main(int argc,char** argv){
	HDFFile test= HDFFile(argv[1]);
	test.setTranspose(true);
	//the first sample for bunch 255
	std::cout<<"data: "<<test[255].get()[0]<<std::endl;

}
```