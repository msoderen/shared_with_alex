
//standard includes
#include <atomic>
#include <iostream>
#include <sstream>


//extra includes
#include <boost/interprocess/ipc/message_queue.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/permissions.hpp>

//own include
#include "HDFLib.h"


using namespace HDFLib;
using namespace std;

struct HDFWriterSetup{
	uint32_t channel_index; //set in constructor
	uint32_t compression_ratio; //set from child process
	uint32_t columns; //set in setter
	uint32_t rows; //set in constructor
	uint32_t default_header=0;//set in setter
	uint32_t default_sourceID=0;//set in setter
	uint32_t default_blockSize=0;//set in setter
	uint32_t default_tagBits=0;//set in setter
	uint32_t default_cyclenumber=0;//set in setter
	uint16_t default_dataWordSize=0;//set in setter
	uint16_t default_dataPerBunch=0;//set in setter
	uint32_t default_reserved=0;//set in setter
	bool running=false; //set from child process
	char file_prefix[32];//set in setter
	char beam[32];//set in setter
	char plane[32];//set in setter
	char message_queue_name[32];  //set in constructor
	char card_name[32];
	char locations[32][32]; //set in setter

}__attribute__((__packed__));


class FileProcessChild{
public:
	FileProcessChild(const std::string& shared_memory_name):_shared_memory_name(shared_memory_name){
		try{
			_managed_shm = boost::interprocess::managed_shared_memory(boost::interprocess::open_only, _shared_memory_name.c_str());
			}		
		catch (const std::exception &e){
			std::ostringstream temp;
			temp<<"could not open shared memory with error: "<<e.what();
			throw std::runtime_error(temp.str());
		}	

		std::pair<struct HDFWriterSetup *,std::size_t> ret = _managed_shm.find<struct HDFWriterSetup>("HDF_writer_setup");
		if(std::get<0>(ret)==NULL){
			throw std::runtime_error("Could not find HDF_writer_setup in shared memory");
		}
		_setup=std::get<0>(ret);
		_channel_index=_setup->channel_index;
		_columns=_setup->columns;
		_rows=_setup->rows;
		_default_header=_setup->default_header;
		_default_sourceID=_setup->default_sourceID;
		_default_blockSize=_setup->default_blockSize;
		_default_tagBits=_setup->default_tagBits;
		_default_cyclenumber=_setup->default_cyclenumber;
		_default_dataWordSize=_setup->default_dataWordSize;
		_default_reserved=_setup->default_reserved;
		_file_prefix=_setup->file_prefix;
		_beam=_setup->beam;
		_card=_setup->card_name;
		_plane=_setup->plane;
		_message_queue_name=_setup->message_queue_name;
		for(int i=0;i<32;i++){
			std::string temp=_setup->locations[i];
			if(temp.size()!=0){
				_locations.push_back(temp);
			}
			else{
				break;
			}
		}
		_message_queue_size=_rows*sizeof(uint64_t);
		std::cout<<"Child process with: \n"<<
		"channel_index: "<<_channel_index<<"\n"<<
		"columns: "<<_columns<<"\n"<<
		"rows: "<<_rows<<"\n"<<
		"default_header: "<<_default_header<<"\n"<<
		"default_sourceID: "<<_default_sourceID<<"\n"<<
		"default_blocksize: "<<_default_blockSize<<"\n"<<
		"default_tagbits: "<<_default_tagBits<<"\n"<<
		"default_cyclenumber: "<<_default_cyclenumber<<"\n"<<
		"default_dataWordSize: "<<_default_dataWordSize<<"\n"<<
		"default_reserved: "<<_default_reserved<<"\n"<<
		"file_prefix: "<<_file_prefix<<"\n"<<
		"beam: "<<_beam<<"\n"<<
		"plane: "<<_plane<<"\n"<<
		"card: "<<_card<<"\n"<<
		"message_queue_name: "<<_message_queue_name<<std::endl;
		try{
		 	_message_queue =new boost::interprocess::message_queue(boost::interprocess::open_only,_message_queue_name.c_str()); 	
			}	
		catch (const std::exception &e){
			std::ostringstream temp;
			temp<<"could not open message queue with error: "<<e.what();
			throw std::runtime_error(temp.str());
		}

		_offset_buffer=reinterpret_cast<uint64_t*>(malloc(_message_queue_size));

	}

	std::vector<uint64_t> getOffsets(){
		std::size_t message_size;
		unsigned message_prio;
		_message_queue->receive(_offset_buffer,_message_queue_size,message_size,message_prio);
		if(_message_queue_size!=message_size){
			throw std::runtime_error("The size of the message received does not match what we expect");
		}
		std::vector<uint64_t> dest(_offset_buffer, _offset_buffer + _rows);
		return dest;

	}
	~FileProcessChild(){
		delete _message_queue;
		free(_offset_buffer);
	}
protected:
private:
	FileProcessChild()=delete;
	FileProcessChild(const FileProcessChild&)=delete;
	const std::string _shared_memory_name;
	boost::interprocess::managed_shared_memory _managed_shm;

	boost::interprocess::message_queue* _message_queue=NULL;
	uint64_t* _offset_buffer=NULL;

	HDFWriterSetup* _setup;
	uint32_t _channel_index; //set in constructor
	uint32_t _columns; //set in setter
	uint32_t _rows; //set in constructor
	uint32_t _default_header=0;//set in setter
	uint32_t _default_sourceID=0;//set in setter
	uint32_t _default_blockSize=0;//set in setter
	uint32_t _default_tagBits=0;//set in setter
	uint32_t _default_cyclenumber=0;//set in setter
	uint16_t _default_dataWordSize=0;//set in setter
	uint16_t _default_dataPerBunch=0;//set in setter
	uint32_t _default_reserved=0;//set in setter
	std::string _file_prefix;//set in setter
	std::string _beam;//set in setter
	std::string _plane;//set in setter
	std::string _card;
	std::string _message_queue_name;  //set in constructor
	std::size_t _message_queue_size=0;
	std::vector<std::string> _locations; //set in setter
};

int main(int argc, char* argv[]){
    std::vector <std::string> arguments;
    for (int i = 1; i < argc; ++i) {
            arguments.push_back(argv[i]); // Add all but the last argument to the vector.
     }
     if(arguments.size()<1){
        throw std::runtime_error ("Usage: HDFWriter sharedMemoryName, example: HDFWriter 1b00_0 ");
     }
     std::cout<<"HDFFile_test3 with args "<<arguments[0]<<std::endl;
     FileProcessChild* process =new FileProcessChild(arguments[0]);
     while(true){
     	process->getOffsets();
     	std::cout<<"recevied data"<<std::endl;
     }

}
