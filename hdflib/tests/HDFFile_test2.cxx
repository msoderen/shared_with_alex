#include "HDFLib.h"
#include <cassert>
#include <thread>
using namespace HDFLib;
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <chrono>
#include <random>
void threadFunc(){
	//time to seed rng
	std::chrono::time_point<std::chrono::high_resolution_clock> now = std::chrono::high_resolution_clock::now();
	auto duration = now.time_since_epoch();
	auto nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(duration);
	std::size_t seed= nanoseconds.count();

	//init rng
	int stringLength = 3;
	std::default_random_engine generator{seed};
	std::uniform_int_distribution<std::size_t> distribution(0,stringLength);

	//buffer for data
	unsigned size=65536;
	int16_t* data =reinterpret_cast<int16_t*>(malloc(size*3564*sizeof(int16_t) ) );
	for(int i=0 ;i<100;i++){

		std::ostringstream filename;
		filename<<"/obsbox/slow/disk"<<distribution(generator)<<"/"<<"test";
		for(int j=0;j<5;j++){
			filename<<distribution(generator);
		}
		filename<<".h5";
		HDFFile test= HDFFile(filename.str());
		test.setTurns(size);
		test.open(CREATE);
		//test data

		for (std::size_t k=0;k<size*3564;k++){
			data[k]=k%(3565*10);
		}
		test.setData(data);
		test.close();

	}


}

int main(){
	std::vector<std::thread*> threads;
	for(int i=0;i<10;i++){
		threads.push_back(new std::thread(threadFunc));
	}
	for(auto it: threads){
		it->join();
	}
}
