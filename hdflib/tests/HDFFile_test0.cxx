#include "HDFLib.h"
#include <cassert>
using namespace HDFLib;
int main(){
	while(true){
	HDFFile test= HDFFile("test.h5");
	test.setTurns(1024);
	test.open(CREATE);

	//test data
	int16_t* data =reinterpret_cast<int16_t*>(malloc(1024*3564*sizeof(int16_t) ) );
	for (std::size_t i=0;i<1024*3564;i++){
		data[i]=i%30000;
	}
	test.setData(data);

	//test header
	uint32_t headers[1024];
	test.setDefaultHeader(1);
	for(int i =0 ;i<1024;i++){
		headers[i]=1;
	}
	test.setHeaders(headers);

	//test SourceID
	uint32_t sourceIDs[1024];
	test.setDefaultSourceID(2);
	for(int i =0 ;i<1024;i++){
		sourceIDs[i]=2;
	}
	test.setSourceIDs(sourceIDs);
	
	//test blocksize
	uint32_t blockSizes[1024];
	test.setDefaultBlockSize(3);
	for(int i =0 ;i<1024;i++){
		blockSizes[i]=3;
	}
	test.setBlockSizes(blockSizes);

	//test turncounters
	uint32_t turnCounters[1024];
	for(int i =0 ;i<1024;i++){
		turnCounters[i]=i;
	}
	test.setTurnCounters(turnCounters);

	//test tagbits
	uint32_t tagBits[1024];
	test.setDefaultTagBit(4);
	for(int i =0 ;i<1024;i++){
		tagBits[i]=4;
	}
	test.setTagBits(tagBits);

	//test  cyclenumber
	uint32_t cycleNumbers[1024];
	test.setDefaultCycleNumber(5);
	for(int i =0 ;i<1024;i++){
		cycleNumbers[i]=5;
	}
	test.setCycleNumbers(cycleNumbers);

	//test datawordsize
	uint16_t dataWordSize[1024];
	test.setDefaultDataWordSize(6);
	for(int i =0 ;i<1024;i++){
		dataWordSize[i]=6;
	}
	test.setDataWordSizes(dataWordSize);

	//test dataperbunches
	uint16_t dataPerBunch[1024];
	test.setDefaultDataPerBunch(7);
	for(int i =0 ;i<1024;i++){
		dataPerBunch[i]=7;
	}
	test.setDataPerBunches(dataPerBunch);

	//test  cyclenumber
	uint32_t reserves[1024];
	test.setDefaultReserved(8);
	for(int i =0 ;i<1024;i++){
		reserves[i]=8;
	}
	test.setReserves(reserves);

	//test  CRC32
	uint32_t CRC32[1024];
	for(int i =0 ;i<1024;i++){
		CRC32[i]=9;
	}
	test.setCRC32s(CRC32);

	bool ALLOK[1024];
	for(int i =0 ;i<1024;i++){
		ALLOK[i]=true;
	}
	test.setCRC32sOK(ALLOK);

	test.setAPECTagsOK(ALLOK);

	test.setNotInTablesOK(ALLOK);

	test.setDisparitiesOK(ALLOK);

	uint64_t triggerTime=10;
	test.setTriggerTime(triggerTime);

	uint64_t freezeTime=11;
	test.setFreezeTime(freezeTime);

	uint64_t saveTime=12;
	test.setSaveTime(saveTime);

	std::string triggerType="manual";
	test.setTriggerType(triggerType);

	uint32_t cycleId =13;
	test.setCycleId(cycleId);

	std::string cycleName="LHCFULLCYCLE";
	test.setCycleName(cycleName);

	uint64_t cycleStamp=14;
	test.setCycleStamp(cycleStamp);

	std::string extraCondition="NONE";
	test.setExtraCondition(extraCondition);

	uint64_t timeStamp=15;
	test.setTimeStamp(timeStamp);

	std::string timingDomainName="LHC";
	test.setTimingDomainName(timingDomainName);

	test.close();
	HDFFile test1=HDFFile("test.h5");
	test1.open();

	assert(memcmp(data,test1.getData().get(),1024*3564*sizeof(uint16_t))==0);
	free(data);

	//readback headers
	assert(memcmp(headers,test1.getHeaders().get(),1024*sizeof(uint32_t))==0);
	assert(memcmp(ALLOK,test1.getHeadersOK().get(),1024*sizeof(bool))==0);
	assert(test1.getHeaderOK());

	//readback sourceIDs
	assert(memcmp(sourceIDs,test1.getSourceIDs().get(),1024*sizeof(uint32_t))==0);
	assert(memcmp(ALLOK,test1.getSourceIDsOK().get(),1024*sizeof(bool))==0);
	assert(test1.getSourceIDOK());	

	assert(memcmp(blockSizes,test1.getBlockSizes().get(),1024*sizeof(uint32_t))==0);
	assert(memcmp(ALLOK,test1.getBlockSizesOK().get(),1024*sizeof(bool))==0);
	assert(test1.getBlockSizeOK());

	assert(memcmp(turnCounters,test1.getTurnCounters().get(),1024*sizeof(uint32_t))==0);
	assert(memcmp(ALLOK,test1.getTurnCountersOK().get(),1024*sizeof(bool))==0);
	assert(test1.getTurnCounterOK());

	assert(memcmp(tagBits,test1.getTagBits().get(),1024*sizeof(uint32_t))==0);
	assert(memcmp(ALLOK,test1.getTagBitsOK().get(),1024*sizeof(bool))==0);
	assert(test1.getTagBitOK());

	assert(memcmp(cycleNumbers,test1.getCycleNumbers().get(),1024*sizeof(uint32_t))==0);
	assert(memcmp(ALLOK,test1.getCycleNumbersOK().get(),1024*sizeof(bool))==0);
	assert(test1.getCycleNumberOK());

	assert(memcmp(dataWordSize,test1.getDataWordSizes().get(),1024*sizeof(uint16_t))==0);
	assert(memcmp(ALLOK,test1.getDataWordSizesOK().get(),1024*sizeof(bool))==0);
	assert(test1.getDataWordSizeOK());

	assert(memcmp(dataPerBunch,test1.getDataPerBunches().get(),1024*sizeof(uint16_t))==0);
	assert(memcmp(ALLOK,test1.getDataPerBunchesOK().get(),1024*sizeof(bool))==0);
	assert(test1.getDataPerBunchOK());

	assert(memcmp(reserves,test1.getReserves().get(),1024*sizeof(uint32_t))==0);
	assert(memcmp(ALLOK,test1.getReservesOK().get(),1024*sizeof(bool))==0);
	assert(test1.getReserveOK());

	assert(memcmp(CRC32,test1.getCRC32s().get(),1024*sizeof(uint32_t))==0);
	assert(memcmp(ALLOK,test1.getCRC32sOK().get(),1024*sizeof(bool))==0);
	assert(test1.getCRC32OK());	

	assert(memcmp(ALLOK,test1.getAPECTagsOK().get(),1024*sizeof(bool))==0);
	assert(test1.getAPECTagOK());

	assert(memcmp(ALLOK,test1.getNotInTablesOK().get(),1024*sizeof(bool))==0);
	assert(test1.getNotInTableOK());

	assert(memcmp(ALLOK,test1.getDisparitiesOK().get(),1024*sizeof(bool))==0);
	assert(test1.getDisparityOK());

	assert(test1.getTriggerTime()==triggerTime);

	assert(test1.getFreezeTime()==freezeTime);

	assert(test1.getSaveTime()==saveTime);

	assert(test1.getTriggerType()==triggerType);

	assert(test1.getCycleId()==cycleId);

	assert(test1.getCycleName()==cycleName);

	assert(test1.getCycleStamp()==cycleStamp);

	assert(test1.getExtraCondition()==extraCondition);

	assert(test1.getTimeStamp()==timeStamp);

	assert(test1.getTimingDomainName()==timingDomainName);
	std::cout<<"passed"<<std::endl;
	std::cout<<"Compression ratio: "<<test1.getCompressionRatio()<<std::endl;
	test1.close();
	return 0;
	//boost::filesystem::remove("test.h5");
}
}
