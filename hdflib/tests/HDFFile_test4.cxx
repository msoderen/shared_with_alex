//standard includes
#include <atomic>
#include <iostream>
#include <sstream>
#include <chrono>
#include <random>

//extra includes
#include <boost/interprocess/ipc/message_queue.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/permissions.hpp>
#include <boost/process.hpp>

//own include
#include "HDFLib.h"


using namespace HDFLib;
using namespace std;

struct HDFWriterSetup{
	uint32_t channel_index; //set in constructor
	uint32_t compression_ratio; //set from child process
	uint32_t columns; //set in setter
	uint32_t rows; //set in constructor
	uint32_t default_header=0;//set in setter
	uint32_t default_sourceID=0;//set in setter
	uint32_t default_blockSize=0;//set in setter
	uint32_t default_tagBits=0;//set in setter
	uint32_t default_cyclenumber=0;//set in setter
	uint16_t default_dataWordSize=0;//set in setter
	uint16_t default_dataPerBunch=0;//set in setter
	uint32_t default_reserved=0;//set in setter
	bool running=false; //set from child process
	char file_prefix[32];//set in setter
	char beam[32];//set in setter
	char plane[32];//set in setter
	char message_queue_name[32];  //set in constructor
	char card_name[32];
	char locations[32][32]; //set in setter

}__attribute__((__packed__));



class FileProcess{
public:
	FileProcess(const std::string& binary,const std::string& card,const uint32_t& channel,const uint32_t& rows): _binary(binary),_card(card),_channel(channel),_rows(rows){
		{
			std::ostringstream temp;
			std::size_t random_number=getRandomNumber();
			temp<<_card<<"_"<<_channel<<"_"<<random_number<<"_shm";
			_shared_memory_name=temp.str();
			temp.str("");
			temp<<_card<<"_"<<_channel<<"_"<<random_number<<"_mq";
			_message_queue_name=temp.str();
		}


		_shared_memory_size=pow2Ceil(sizeof(struct HDFWriterSetup));
		_permissions.set_unrestricted();

		//create shared memory
		try{
			_managed_shm = boost::interprocess::managed_shared_memory(boost::interprocess::create_only, _shared_memory_name.c_str(),_shared_memory_size, 0, _permissions);
		}
		catch (const std::exception &e){
			std::ostringstream temp;
			temp<<"could not create shared memory with error: "<<e.what();
			throw std::runtime_error(temp.str());
		}
		try{
			_setup = _managed_shm.construct<struct HDFWriterSetup>("HDF_writer_setup") ();
		}		
		catch (const std::exception &e){
			std::ostringstream temp;
			temp<<"could not allocate HDFWriterSetup in shared memory with error: "<<e.what();
			throw std::runtime_error(temp.str());
		}
		if(_setup==NULL){
			throw std::runtime_error("could not allocate HDFWriterSetup in shared memory, returned NULL pointer");
		}

		_setup->channel_index=_channel;
		_setup->rows=_rows;
		setCstr(_setup->message_queue_name,_message_queue_name);
		setCstr(_setup->card_name,_card);

		_message_queue_size=_rows*sizeof(uint64_t);

		try{
			_message_queue= new boost::interprocess::message_queue(boost::interprocess::create_only ,_message_queue_name.c_str(),128,_message_queue_size);
		}		
		catch (const std::exception &e){
			std::ostringstream temp;
			temp<<"could not create message queue with error: "<<e.what();
			throw std::runtime_error(temp.str());
		}

	}
	~FileProcess(){
		_child_process.terminate();
		boost::interprocess::shared_memory_object::remove(_shared_memory_name.c_str());
		boost::interprocess::message_queue::remove(_message_queue_name.c_str());
		delete _message_queue;

	}

	void start(){
		_child_process= boost::process::child(_binary,_shared_memory_name);
		if(!_child_process.running()){
			std::ostringstream temp;
			temp<<"child process for binary "<<_binary<<" and shared memory "<<_shared_memory_name<<" did not start";
			throw std::runtime_error(temp.str());
		}
	}

	void wait(){
		_child_process.wait();
	}

	void pushOffsets(const std::vector<uint64_t>& offsets){
		if(!_child_process.running()){
			throw std::runtime_error("pushing offsets while child process is not running");
		}
		if (offsets.size()!=_rows){
			throw std::runtime_error("rows and size of vector in pushOffsets does not match");
		}
		   	_message_queue->send(offsets.data(),_rows*sizeof(uint64_t),0);
		

	}

	

	void setColumns(uint32_t columns){
		_setup->columns=columns;
	}
	void setDefaultHeader(uint32_t default_header){
		_setup->default_header=default_header;
	}
	void setDefaultSourceID(uint32_t default_sourceID){
		_setup->default_sourceID=default_sourceID;
	}
	void setDefaultBlockSize(uint32_t default_blockSize){
		_setup->default_blockSize=default_blockSize;
	}
	void setDefaultTagBits(uint32_t default_tagBits){
		_setup->default_tagBits=default_tagBits;
	}
	void setDefaultCycleNumber(uint32_t default_cyclenumber){
		_setup->default_cyclenumber=default_cyclenumber;
	}
	void setDefaultDataWordSize(uint16_t default_dataWordSize){
		_setup->default_dataWordSize=default_dataWordSize;
	}
	void setDefaultDataPerBunch(uint16_t default_dataPerBunch){
		_setup->default_dataPerBunch=default_dataPerBunch;
	}
	void setDefaultReserved(uint32_t default_reserved){
		_setup->default_reserved=default_reserved;
	}
	void setFilePrefix(const std::string& file_prefix){
		setCstr(_setup->file_prefix,file_prefix);
	}
	void setBeam(const std::string& beam){
		setCstr(_setup->beam,beam);
	}
	void setPlane(const std::string& plane){
		setCstr(_setup->plane,plane);
	}
	void setLocations(const std::vector<std::string>& locations){
		std::size_t index=0;
		for(auto it : locations){
			setCstr(_setup->locations[index],it);
			index++;
		}

	}
protected:
private:
	FileProcess() = delete;
	FileProcess(const FileProcess&)=delete;
	std::size_t getRandomNumber(){
		std::chrono::time_point<std::chrono::high_resolution_clock> now = std::chrono::high_resolution_clock::now();
		auto duration = now.time_since_epoch();
		auto nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(duration);
		std::size_t seed= nanoseconds.count();

		//init rng
		int stringLength = 10000;
		std::default_random_engine generator{seed};
		std::uniform_int_distribution<std::size_t> distribution(0,stringLength);
		return distribution(generator);
	}
	unsigned pow2Ceil(unsigned v){
		v--;
		v |= v >> 1;
		v |= v >> 2;
		v |= v >> 4;
		v |= v >> 8;
		v |= v >> 16;
		v++;	
		return v;
	}

	void setCstr(char* cstr,const std::string& stdstr){
		strcpy(cstr, stdstr.c_str());
	}

	const std::string _binary;
	const std::string _card;
	const uint32_t _channel;
	const uint32_t _rows;
	std::string _shared_memory_name;
	std::string _message_queue_name;
	boost::interprocess::managed_shared_memory _managed_shm;
	std::size_t _shared_memory_size;
	boost::interprocess::permissions _permissions;
	struct HDFWriterSetup *_setup;

	boost::interprocess::message_queue* _message_queue;
	std::size_t _message_queue_size=0;

	boost::process::child _child_process;
};


int main(int argc, char* argv[]){

	FileProcess* process= new FileProcess("/home/msoderen/repositories/hdflib/tests/HDFFile_test3","1b00",1,4096);
	process->setColumns(3564);
	process->setDefaultHeader(1331852081);
	process->setDefaultSourceID(0);
	process->setDefaultBlockSize(7128);
	process->setDefaultTagBits(4096);
	process->setDefaultCycleNumber(2147483648);
	process->setDefaultDataWordSize(0);
	process->setDefaultDataPerBunch(0);
	process->setDefaultReserved(0);
	process->setFilePrefix("test1");
	process->setBeam("B1");
	process->setPlane("horizontal");
	std::vector<std::string> locations;
	locations.push_back("/obsbox/slow/disk1");
	locations.push_back("/obsbox/slow/disk2");
	locations.push_back("/obsbox/slow/disk3");
	process->setLocations(locations);
	process->start();
	std::vector<uint64_t> offsets(4096);
	for(int i =0;i<100;i++){
		process->pushOffsets(offsets);
		usleep(500000);
	}
	delete process;

}
