#include "HDFLib.h"
#include <cassert>
using namespace HDFLib;
int main(){
	unsigned size=65536;
	HDFFile test= HDFFile("test.h5");
	//test.setAttributesEnabled(false);
	test.setTurns(size);
	test.open(CREATE);
	//test data
	int16_t* data =reinterpret_cast<int16_t*>(malloc(size*3564*sizeof(int16_t) ) );
	for (std::size_t i=0;i<size*3564;i++){
		data[i]=i%(3565*10);
	}
	test.setData(data);
	std::cout<<test.getWriteSpeed()<<"MB/s"<<std::endl;
}
