import os
import datetime
from shutil import copyfile

def compare_files(file0, file1):
	temp0=file0.split("_")
	temp1=file1.split("_")
	#print(temp0)
	#print(temp1)
	if "ADT" in temp0[1]:
		return False
	if "ADT" in temp1[1]:
		return False
			
	#fill
	if temp0[0]!=temp1[0]:
		return False
	#buffer	
	if temp0[1]!=temp1[1]:
		return False	
	#plane	
	if temp0[2]!=temp1[2]:
		return False	
	#pickup	
	if temp0[3]==temp1[3]:
		return False	
	#date	
	if temp0[4]!=temp1[4]:
		return False
	pt0=datetime.datetime.strptime(temp0[5][:-3],'%Hh%Mm%Ss')
	pt1=datetime.datetime.strptime(temp1[5][:-3],'%Hh%Mm%Ss')
	ts0 = pt0.second+pt0.minute*60+pt0.hour*3600
	ts1 = pt1.second+pt1.minute*60+pt1.hour*3600
	if abs(ts0-ts1)>6:
		return False
	return True
if compare_files("07355_64k_B1H_Q7_20181025_21h17m01s.h5","07355_64k_B1H_Q8_20181025_21h17m00s.h5"):
	print("match")

with open("other_directories.txt") as f:
	#for every fill
	for line in f:
		print(line)
		#for every file in that fill
		files=[]
		for data_file in os.listdir(line[:-1]):
			file_info=os.stat(line[:-1]+"/"+data_file)
			#empty file			
			if file_info.st_size<=37034:
				continue
			files.append(data_file)
		#print(files)
		
		correct_files=[]
		for element in files:
			counter=0
			for element1 in files:
				if compare_files(element,element1):
					counter+=1
			if counter >=2:
				correct_files.append(element)
		print(correct_files)
		for element in correct_files:
			temp0=file0.split("_")
			fill=temp0[0]
			plane=temp0[2]
			if not os.path.exists("/nfs/cs-ccr-adtobsnfs/lhc_adtobsbox_data/tune_data"+):
    				os.makedirs(directory)
