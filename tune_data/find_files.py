import re
import os
import datetime
import shutil

ADTDATALOCATION='/nfs/cs-ccr-adtobsnfs/lhc_adtobsbox_data'

def compare_files(file0, file1):
	temp0=file0.split("_")
	temp1=file1.split("_")
	if len(temp0)<6 or len(temp1)<6:
		return False
	#print(temp0)
	#print(temp1)
	if "ADT" in temp0[1]:
		return False
	if "ADT" in temp1[1]:
		return False
			
	#fill
	if temp0[0]!=temp1[0]:
		return False
	#buffer	
	if temp0[1]!=temp1[1]:
		return False	
	#plane	
	if temp0[2]!=temp1[2]:
		return False	
	#pickup	
	if temp0[3]==temp1[3]:
		return False	
	#date	
	if temp0[4]!=temp1[4]:
		return False
	pt0=datetime.datetime.strptime(temp0[5][:-3],'%Hh%Mm%Ss')
	pt1=datetime.datetime.strptime(temp1[5][:-3],'%Hh%Mm%Ss')
	ts0 = pt0.second+pt0.minute*60+pt0.hour*3600
	ts1 = pt1.second+pt1.minute*60+pt1.hour*3600
	if abs(ts0-ts1)>6:
		return False
	return True

def count_pickups(data):
	counter=0
	if any("Q7" in s for s in data):
		counter+=1
	if any("Q8" in s for s in data):
		counter+=1	
	if any("Q9" in s for s in data):
		counter+=1
	if any("Q10" in s for s in data):
		counter+=1
	return counter

regex=re.compile("^[0-9]{4}$")
subdirs=[x for x in os.listdir(ADTDATALOCATION) if regex.search(x) and int(x)>=7335]
subdirs=sorted(subdirs)
for fill in subdirs:
	#print(fill)
	for bufferType in os.listdir(ADTDATALOCATION+"/"+fill):
		#skip injection		
		if bufferType=="injection_data":
			continue
		#print(bufferType)
		path=ADTDATALOCATION+"/"+fill+"/"+bufferType
		onlyfiles = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
		#print(onlyfiles)
		#for every file in this directory
		counter=0
		for i in range(len(onlyfiles)):
			file_info=os.stat(path+"/"+onlyfiles[i])
			#skip small files
			if file_info.st_size<=37034:
				continue
			temp=[onlyfiles[i]]
			#compare it to all other files
			for j in range(i,len(onlyfiles)):
				#the two files match
				if compare_files(onlyfiles[i],onlyfiles[j]):
					temp.append(onlyfiles[j])
			if count_pickups(temp)>2:
				print(temp)
				directory="/nfs/cs-ccr-adtobsnfs/lhc_adtobsbox_data/tune_data/"+fill+"/match"+str(counter)
				counter+=1
				if not os.path.exists(directory):
    					os.makedirs(directory)
				for element in temp:
					shutil.copyfile(path+"/"+element,directory+"/"+element)
				
