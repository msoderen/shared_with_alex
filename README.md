# shared_with_alex

# hdflib 
contains Python and C++ library to easily read the data from our HDF files. You can ignore the python library in this folder because it is incomplete

# hilbert_transform
Contains python code to open HDF files (plotlib.py) and code to apply a hilbert transform (7 tap FIR filter with coefficient for a flat frequency response in the 0.25-035 normalized frequency span)

# tune_data
Contains HDF files from RUN2 of LHC. These are the files we have with synchronized data from 3 or more pickups. I need to extract the phase between the pickups for the different files

# tuneextractioncuda

Contains cuda code(main.cu) to do a NUFFT-3. It is pretty efficiently implemented. There are also a mixture of different scripts that can be ignored

