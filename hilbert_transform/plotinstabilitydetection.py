# -*- coding: utf-8 -*-
import plotlib
import numpy as np
import matplotlib.pyplot as plt
import math
import argparse

def plotfileFunc(filename,bunch,window,threshold):
  avglen=window
  plt.rcParams.update({"font.size":12})
  file=plotlib.PlotFile(filename)
  if not file.open:
    raise RuntimeError("File "+ filename+" could not be opened")
  element=file.datasets[0]
  tempdata=element.data[bunch]

  #plot raw data
  f, axarr=plt.subplots(1,figsize=(8.27, 11.69/4), dpi=100)
  ax1=axarr
  ax1.grid(True)
  ax1.set_title("Raw bunch positional data")
  ax1.set_ylabel("Bunch position [A.U.]")
  ax1.set_xlabel("Turn number")
  ax1.plot(range(len(tempdata)),tempdata)
  ax1.set_xlim([0,len(tempdata)])
  ax1.set_ylim([min(tempdata),max(tempdata)])
  plt.tight_layout()
  plt.subplots_adjust(left=0.09, right=0.99, top=0.91, bottom=0.19)

  #plot notch data
  notchdata=np.convolve(tempdata,np.array([1,-1]))[1:-1]
  f, axarr=plt.subplots(1,figsize=(8.27, 11.69/4), dpi=100)
  ax1=axarr
  ax1.grid(True)
  ax1.set_title("Data after notch filter")
  ax1.set_ylabel("Bunch position [A.U.]")
  ax1.set_xlabel("Turn number")
  ax1.plot(range(len(notchdata)),notchdata)
  ax1.set_xlim([0,len(notchdata)])
  ax1.set_ylim([min(notchdata),max(notchdata)])
  plt.tight_layout()
  plt.subplots_adjust(left=0.09, right=0.99, top=0.91, bottom=0.19)

  #amplitude
  I=np.convolve(notchdata,np.array([-0.0906,-0.0197,-0.5941,0,0.5941,0.0197,0.0906]))[6:-1]
  Q=np.convolve(notchdata,np.array([0,0,0,1,0,0,0]))[6:-1]
  amp=[]
  for i in range(len(I)):
    amp.append(math.sqrt(pow(I[i],2)+pow(Q[i],2)))
  f, axarr=plt.subplots(1,figsize=(8.27, 11.69/4), dpi=100)
  ax1=axarr
  ax1.grid(True)
  ax1.set_title("Instantaneous oscillation amplitude")
  ax1.set_ylabel("Amplitude [A.U.]")
  ax1.set_xlabel("Turn number")
  ax1.plot(range(len(amp)),amp)
  ax1.set_xlim([0,len(amp)])
  ax1.set_ylim([min(amp),max(amp)])
  plt.tight_layout()
  plt.subplots_adjust(left=0.09, right=0.99, top=0.91, bottom=0.19)

  #average
  padded=np.pad(amp,int((len(tempdata)-len(amp))/2),mode='symmetric')
  avg=np.mean(padded.reshape(-1,avglen),axis=1)
  minavg=min(avg)*0.9
  f, axarr=plt.subplots(1,figsize=(8.27, 11.69/4), dpi=100)
  ax1=axarr
  ax1.grid(True)
  ax1.set_title("Moving average of instantaneous oscillation amplitude")
  ax1.set_ylabel("Amplitude [A.U.]")
  ax1.set_xlabel("Turn number")
  bar=ax1.bar(range(int(avglen/2),len(tempdata)+int(avglen/2),avglen),avg,alpha=0.5,color=(0,0,0.5),width=avglen,zorder=10,label="Moving average window")
  ax1.set_xlim([0,len(tempdata)])
  ax1.set_ylim([minavg,max(avg)+(max(avg)-minavg)*0.3])

  avgcpy=np.array(avg,copy=True)
  avgcpy[0]=minavg
  for x in range(1,len(avg),1):
    if avg[x-1]*1.2>avg[x]:
      avgcpy[x]=minavg
  bar1=ax1.bar(range(int(avglen/2),len(tempdata)+int(avglen/2),avglen),avgcpy,alpha=0.5,color=(1,0,0),width=avglen,zorder=10,label="Trigger")
  ax1.legend(bbox_to_anchor=(0.430, 0.905),bbox_transform=plt.gcf().transFigure,labelspacing=0)
  plt.tight_layout()
  plt.subplots_adjust(left=0.09, right=0.99, top=0.91, bottom=0.19)
  plt.show()
  file.close()

if __name__ == "__main__":
  parser=argparse.ArgumentParser()
  parser.add_argument("-bunch","-b", nargs=1, help='<Required> which bunch to analyze', type=int,required=True)
  parser.add_argument("-filename","-f",required=True,type=str)
  parser.add_argument("-window","-w", nargs=1, help='<Required> window length', type=int,default=256)
  parser.add_argument("-threshold","-t", nargs=1, help='<Required> which threshold to use', type=float,default=1.8)
  args = parser.parse_args()
  num=args.window[0]
  if not (num !=0 and ((num&(num-1))==0)):
    raise RuntimeError("Window length not a power of two")
  if args.bunch[0]<0 or args.bunch[0]>3563:
    raise RuntimeError("Bunchindex does not make sence")
  plotfileFunc(args.filename,args.bunch[0],args.window[0],args.threshold)