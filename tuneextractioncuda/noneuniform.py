import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft
import nfft
PICKUPS=4
SAMPLES=11245
phases=[0,0.001,0.002,0.003]

x =[]
for i in range(SAMPLES):
	x.append( (1.0/float(SAMPLES)) * (float(i)+phases[0]) )
	x.append( (1.0/float(SAMPLES)) * (float(i)+phases[1]) )
	x.append( (1.0/float(SAMPLES)) * (float(i)+phases[2]) )
	x.append( (1.0/float(SAMPLES)) * (float(i)+phases[3]) )

#x=sample times [0-1]
y = np.sin(0.31001*2.0*np.pi*np.array(x)*float(SAMPLES))
X=nfft.ndft(x,y)
plt.plot(X)
plt.show()