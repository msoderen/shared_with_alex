import matplotlib.pyplot as plt
import numpy as np

x=[]
with open('test.txt') as f:
    lines = f.readlines()
    x = [float(line) for line in lines]

fig = plt.figure()

ax1 = fig.add_subplot(111)

y=np.linspace(0.25,0.35,1024)
print(y)
ax1.plot(y,x, c='r', label='the data')
plt.show()