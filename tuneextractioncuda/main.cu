#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cmath>
#include <cuda_profiler_api.h>
#include <vector>
#include <random>
#include <map>
#include <chrono>
#include <iostream>


#ifdef PLOT
	#include "matplotlibcpp.h"
	namespace plt = matplotlibcpp;
#endif

//#include <helper_math.h>


#define debug_thread(...)  (threadIdx.x == 0 ? printf(__VA_ARGS__) : 0)
#define DEBUG


#define SAMPLES 11245
#define PICKUPS 4
#if PICKUPS==1
float phases[1]={0};
#elif PICKUPS==2
float phases[2]={0,0.01f};
#elif PICKUPS==3
float phases[3]={0, 0.01f, 0.02f};
#else
	float phases[4]={0, 0.01f, 0.02f, 0.03f};
#endif

#define BUNCHES 3564
#define CUDART_2PI_F 6.283185308f
#define FMAX 0.342f
#define FMIN 0.339f
#define SAMPLESSHARED 12288
#define HANN

/**------------------LIMITATIONS
*SAMPLESHARED must be even dividable with the blocksize
*SAMPLESHARED must be even dividable with 4

**/

// Convenience function for checking CUDA runtime API results
// can be wrapped around any runtime API call. No-op in release builds.
inline
cudaError_t checkCuda(cudaError_t result)
{
#if defined(DEBUG) || defined(_DEBUG)
  if (result != cudaSuccess) {
    cudaGetErrorString(result);
    printf( "CUDA Runtime Error: %s\n", cudaGetErrorString(result));
  //  assert(result == cudaSuccess);
  }
#endif
  return result;
}

//arguments host_buffer: buffer with all bunch data
//host_phase_buffer: buffer with the 4 different phases between the pickups
__global__ void kernel(float *dev_buffer,float *dev_result_buffer,float* dev_phase_buffer) {
	//local buffer for fast access to bunch data
	//must be even dividable by the number of pickups
	__shared__ float shared_buffer_samples[SAMPLESSHARED];
	
	float X=0.0f;
	float Y=0.0f;

//	float4 X4;
//	float4 Y4;
//	X4=X4*Y4;

	//the number of times we need to load data into the shared memory,current conf should mean 4 loads
	int iterations=(SAMPLES*PICKUPS)/SAMPLESSHARED+1;
	float N=__int2float_rd(SAMPLES*PICKUPS);
	//the offset in the global sample buffer for this threads bunch,fixed for ther whole kernel run
	uint32_t offset_bunch=SAMPLESSHARED*iterations*blockIdx.x;
		
	float f=(CUDART_2PI_F)*(FMIN+__int2float_rd (threadIdx.x)*((FMAX-FMIN)/1024.0f));

 	//Multiple iterations since we cant store all samples in the local buffer
	for (int iteration=0;iteration<iterations;iteration++){

		//each thread loads 12 samples	
		uint32_t offset_sample=offset_bunch+SAMPLESSHARED*iteration+threadIdx.x*(SAMPLESSHARED/blockDim.x);
		uint32_t offset_shared=threadIdx.x*(SAMPLESSHARED/blockDim.x);
		shared_buffer_samples[offset_shared]=dev_buffer[offset_sample];
		shared_buffer_samples[offset_shared+1]=dev_buffer[offset_sample+1];
		shared_buffer_samples[offset_shared+2]=dev_buffer[offset_sample+2];
		shared_buffer_samples[offset_shared+3]=dev_buffer[offset_sample+3];
		shared_buffer_samples[offset_shared+4]=dev_buffer[offset_sample+4];
		shared_buffer_samples[offset_shared+5]=dev_buffer[offset_sample+5];
		shared_buffer_samples[offset_shared+6]=dev_buffer[offset_sample+6];
		shared_buffer_samples[offset_shared+7]=dev_buffer[offset_sample+7];
		shared_buffer_samples[offset_shared+8]=dev_buffer[offset_sample+8];
		shared_buffer_samples[offset_shared+9]=dev_buffer[offset_sample+9];
		shared_buffer_samples[offset_shared+10]=dev_buffer[offset_sample+10];
		shared_buffer_samples[offset_shared+11]=dev_buffer[offset_sample+11];
		//------------------------------------------
		//wait for all threads because 
		 __syncthreads();

		 for(int sample_index=0;sample_index<SAMPLESSHARED;sample_index+=PICKUPS){
		 	float sample_index_f0=__int2float_rd(sample_index+iteration*SAMPLESSHARED)+dev_phase_buffer[0];
		 	float sample_index_f1=__int2float_rd(sample_index+iteration*SAMPLESSHARED)+dev_phase_buffer[1];
		 	float sample_index_f2=__int2float_rd(sample_index+iteration*SAMPLESSHARED)+dev_phase_buffer[2];
		 	float sample_index_f3=__int2float_rd(sample_index+iteration*SAMPLESSHARED)+dev_phase_buffer[3];
			#if PICKUPS==1
				shared_buffer_result[threadIdx.x*2]+=shared_buffer_samples[sample_index]*cosf(sample_index_f0*f);
		 		shared_buffer_result[threadIdx.x*2+1]+=shared_buffer_samples[sample_index]*sinf(sample_index_f0*f);
			#elif PICKUPS==2
				shared_buffer_result[threadIdx.x*2]+=shared_buffer_samples[sample_index]*cosf(sample_index_f0*f);
		 		shared_buffer_result[threadIdx.x*2+1]+=shared_buffer_samples[sample_index]*sinf(sample_index_f0*f);
		 		shared_buffer_result[threadIdx.x*2]+=shared_buffer_samples[sample_index+1]*cosf(sample_index_f1*f);
		 		shared_buffer_result[threadIdx.x*2+1]+=shared_buffer_samples[sample_index+1]*sinf(sample_index_f1*f);
			#elif PICKUPS==3
				shared_buffer_result[threadIdx.x*2]+=shared_buffer_samples[sample_index]*cosf(sample_index_f0*f);
		 		shared_buffer_result[threadIdx.x*2+1]+=shared_buffer_samples[sample_index]*sinf(sample_index_f0*f);
		 		shared_buffer_result[threadIdx.x*2]+=shared_buffer_samples[sample_index+1]*cosf(sample_index_f1*f);
		 		shared_buffer_result[threadIdx.x*2+1]+=shared_buffer_samples[sample_index+1]*sinf(sample_index_f1*f);
		 		shared_buffer_result[threadIdx.x*2]+=shared_buffer_samples[sample_index+2]*cosf(sample_index_f2*f);
		 		shared_buffer_result[threadIdx.x*2+1]+=shared_buffer_samples[sample_index+2]*sinf(sample_index_f2*f);
			#else
			 	#ifdef HANN
					X+=shared_buffer_samples[sample_index]*powf(sinf((CUDART_2PI_F*sample_index_f0)/N),2.0)*cosf(sample_index_f0*f)+shared_buffer_samples[sample_index+1]*powf(sinf((CUDART_2PI_F*sample_index_f0)/N),2.0)*cosf(sample_index_f1*f)+shared_buffer_samples[sample_index+2]*powf(sinf((CUDART_2PI_F*sample_index_f0)/N),2.0)*cosf(sample_index_f2*f)+shared_buffer_samples[sample_index+3]*powf(sinf((CUDART_2PI_F*sample_index_f0)/N),2.0)*cosf(sample_index_f3*f);

			 		Y+=shared_buffer_samples[sample_index]*powf(sinf((CUDART_2PI_F*sample_index_f0)/N),2.0)*sinf(sample_index_f0*f)+shared_buffer_samples[sample_index+1]*powf(sinf((CUDART_2PI_F*sample_index_f0)/N),2.0)*sinf(sample_index_f1*f)+shared_buffer_samples[sample_index+2]*powf(sinf((CUDART_2PI_F*sample_index_f0)/N),2.0)*sinf(sample_index_f2*f)+shared_buffer_samples[sample_index+3]*powf(sinf((CUDART_2PI_F*sample_index_f0)/N),2.0)*sinf(sample_index_f3*f);
				#else
					X+=shared_buffer_samples[sample_index]*cosf(sample_index_f0*f)+shared_buffer_samples[sample_index+1]*cosf(sample_index_f1*f)+shared_buffer_samples[sample_index+2]*cosf(sample_index_f2*f)+shared_buffer_samples[sample_index+3]*cosf(sample_index_f3*f);

			 		Y+=shared_buffer_samples[sample_index]*sinf(sample_index_f0*f)+shared_buffer_samples[sample_index+1]*sinf(sample_index_f1*f)+shared_buffer_samples[sample_index+2]*sinf(sample_index_f2*f)+shared_buffer_samples[sample_index+3]*sinf(sample_index_f3*f);

			 	#endif
			#endif
		 }
		 __syncthreads();
	}
	//calculate absolute value
	dev_result_buffer[threadIdx.x+blockIdx.x*1024]=sqrtf(powf(X,2.0)+powf(Y,2.0));

}

int main(){
	float* host_buffer;
	float* host_result_buffer;
	float* dev_buffer;
	std::vector<float> times;
	std::vector<float> data;
	float* dev_phase_buffer;
	float* dev_result_buffer;
	//how many times data has to be loaded into shared memory per bunch
	int shared_memories_per_bunch=ceil(float(SAMPLES)*float(PICKUPS)/float(SAMPLESSHARED));
	//total shared memory per bunch in bytes, SAMPLESHARED*sizeof(float) should be the total amount of shared memory per block
	int memory_per_bunch=shared_memories_per_bunch*SAMPLESSHARED*sizeof(float);
	uint64_t data_size=BUNCHES*memory_per_bunch;

	//-----------------for noise-----------
  	const double mean = 0.0;
    const double stddev = 10.5;
    std::default_random_engine generator;
    std::normal_distribution<double> dist(mean, stddev);
	//-------------------------------------
	//allocate pinned memory

	checkCuda( cudaMallocHost((void**)&host_buffer, data_size) );
	memset(host_buffer,0,data_size);
	checkCuda(cudaMalloc((void**)&dev_buffer, data_size) );

	checkCuda(cudaMalloc((void**)&dev_phase_buffer, 4*sizeof(float)) );

	checkCuda( cudaMallocHost((void**)&host_result_buffer, BUNCHES*1024*sizeof(float)) );
	checkCuda(cudaMalloc((void**)&dev_result_buffer, BUNCHES*1024*sizeof(float)) );
	
	for (uint64_t k=0;k<BUNCHES;k++){
		for (uint64_t i=0;i<SAMPLES;i+=PICKUPS){
			for (int j=0;j<PICKUPS;j++){
				host_buffer[k*memory_per_bunch/sizeof(float)+i+j]=sin(0.341f*2.0f*M_PI*(float(i)+phases[j]))+dist(generator);
				//printf("%lu \n",k*memory_per_bunch/sizeof(float)+i+j);
			}
		}	

	}
	// int counter=0;
	// for (auto element : times){
	// 	host_buffer[counter]=sin(0.3201f*2.0f*M_PI*element);
	// 	data.push_back(host_buffer[counter]);
	// 	counter++;
	// }
	// 	std::map<std::string,std::string> keywords;
	// keywords["marker"]=".";
	//  plt::plot(times,data,keywords);
    //  plt::show();


/**	//generate test data
	for (uint64_t i=0;i<SAMPLES;i+=PICKUPS){
	for (int j=0;j<PICKUPS;j++){
		host_buffer[i+j]=sin((M_PI*2)*0.3201*static_cast<double>(i)+phases[j])+dist(generator);
		}
	}
	**/
	
	checkCuda(cudaMemcpy(dev_buffer, host_buffer, data_size, cudaMemcpyHostToDevice) );

	checkCuda(cudaMemcpy(dev_phase_buffer, phases, 4*sizeof(float), cudaMemcpyHostToDevice) );

	//run kernel 
	cudaProfilerStart();
	auto start = std::chrono::steady_clock::now();	
	 kernel<<<32, 1024>>>(dev_buffer,dev_result_buffer,dev_phase_buffer); 
	 checkCuda(cudaPeekAtLastError());
      checkCuda(cudaDeviceSynchronize());
	 auto duration = std::chrono::duration_cast< std::chrono::milliseconds> (std::chrono::steady_clock::now() - start);
	 std::cout<<"Time taken: "<<duration.count()<<" ms"<<std::endl;

      checkCuda( cudaMemcpy(host_result_buffer, dev_result_buffer, BUNCHES*1024*sizeof(float), cudaMemcpyDeviceToHost) );
      cudaProfilerStop();

      std::vector<float> temp_result;
      std::vector<float> temp_x;
      for(int i =0;i<1024;i++){
      	//printf("%.10f\n",host_result_buffer[i]);
      	temp_result.push_back(host_result_buffer[i+1024*2]);
      	temp_x.push_back(FMIN+(FMAX-FMIN)*static_cast<float>(i)/1024.0);
      }
      #ifdef PLOT
      	plt::plot(temp_x,temp_result);
      	plt::show();
      #endif
      cudaFreeHost(host_buffer);
      cudaFreeHost(host_result_buffer);
      cudaFree(dev_buffer);
      cudaFree(dev_phase_buffer);
      cudaFree(dev_result_buffer);

      cudaDeviceReset();
}