import matplotlib
import matplotlib.pyplot as plt
import numpy as np

# Data for plotting
t = np.arange(0.0, 1.0, 0.01)
s = -np.sin(4 * np.pi * t)
s1=1-np.cos(4 * np.pi * t)
fig, ax = plt.subplots()
ax.plot(t, s)
ax.plot(t, s1)

ax.set(xlabel='time (s)', ylabel='voltage (mV)',
       title='About as simple as it gets, folks')
ax.grid()

fig.savefig("test.png")
plt.show()