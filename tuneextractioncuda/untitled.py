import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import math
from matplotlib.widgets import Slider


SAMPLES=8
PICKUPS=4
phases=[0.0,0.1,0.2,0.3]
tune=0.32001


fig, ax = plt.subplots()

x =[]
y=[]
test=[]
for i in range(SAMPLES):
	for j in range(PICKUPS):
		#x=time 0 0.01 0.02 0.03 1.0
		x.append(  float(i)+phases[j] )
		y.append(np.sin(tune*2.0*np.pi*x[-1])+np.random.normal(0, 0,))

#y = np.sin(tune*2.0*np.pi*np.array(x))
#print(y[:10])
ax.plot(x,y,marker="*")
plt.show()