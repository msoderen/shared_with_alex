import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import math
from matplotlib.widgets import Slider


SAMPLES=32
PICKUPS=4
phases=[0.0,0.1,0.2,0.3]
tune=0.32001


fig, ax = plt.subplots(3,1)

x =[]
y=[]
test=[]
for i in range(SAMPLES):
	for j in range(PICKUPS):
		#x=time 0 0.01 0.02 0.03 1.0
		x.append(  float(i)+phases[j] )
		y.append(np.sin(tune*2.0*np.pi*x[-1])+np.random.normal(0, 0,))

#y = np.sin(tune*2.0*np.pi*np.array(x))
#print(y[:10])
ax[2].plot(x,y,marker="*")
data_x=[]
data_y=[]
for index in range(0,len(y),PICKUPS):
	for j in range(PICKUPS):
		data_x.append(y[index+j]*math.cos(2*np.pi*tune*index/float(PICKUPS) ))
		data_y.append(y[index+j]*math.sin(2*np.pi*tune*index/float(PICKUPS)))
#print(data_x[:10])
#print(data_y[:10])
l,=ax[0].plot(data_x, data_y,"*")

ax[0].set(xlabel='x', ylabel='y',
       title='Time series in polar coordinates')
#plt.xticks(np.arange(0, N, step=1))
ax[0].set_xlim(-2.5, 2.5)
ax[0].set_ylim(-2.5, 2.5)

ax[0].grid()
axfreq = plt.axes([0.25, 0.01, 0.5, 0.03])
sfreq = Slider(axfreq, 'Freq', 0.1, 0.4, valinit=tune)
ax[1].set_xlim(0.1, 0.4)
global max_strength
max_strength=1
def update(val):
	#y = np.sin(tune*2.0*np.pi*np.array(x)*float(SAMPLES))
	data_x=[]
	data_y=[]
	for index in range(len(y)):
		data_x.append(y[index]*np.cos((2.0*np.pi)*val*x[index] ) )
		data_y.append(y[index]*np.sin((2.0*np.pi)*val*x[index] ) )

#	for index in range(0,len(y),PICKUPS):
#		for j in range(PICKUPS):
#			data_x.append(y[index+j]*(math.cos(2*np.pi*val*index/float(PICKUPS) ) ) )
#			data_y.append(y[index+j]*(math.sin(2*np.pi*val*index/float(PICKUPS))  ) )
	#print(data_x)
	#print(data_y)
	strengthx=0.0
	strengthy=0.0
	strength=0.0
	for index in range(len(data_x)):
		strengthx+=data_x[index]
		strengthy+=data_y[index]
	strength=math.sqrt(math.pow(strengthx,2.0)+math.pow(strengthy,2.0))
	ax[1].set_ylim(0, max(ax[1].get_ylim()[1],strength))
	ax[1].plot([val],[strength],"*")
	l.set_ydata(data_y)
	l.set_xdata(data_x)
	fig.canvas.draw_idle()

sfreq.on_changed(update)

#fig.savefig("test.png")
plt.show()
