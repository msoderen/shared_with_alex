#include <cmath>
#include <vector>
#ifdef PLOT
	#include "matplotlibcpp.h"
	namespace plt = matplotlibcpp;
#endif
#include <map>

int main(){
	int SAMPLES = 11245;
	int PICKUPS = 4;
	float phases[4]={0.000,0.1f/SAMPLES,0.2f/SAMPLES,0.3f/SAMPLES};//in radians
	float tune=0.32001;
	//generate test data
	std::vector<float> data;
	std::vector<float> times;
	for (int i =0;i<SAMPLES;i++){
			data.push_back(sin((M_PI*2)*0.3201*static_cast<double>(i)+phases[0]));
			times.push_back((1.0/float(SAMPLES)) * (float(i)+phases[0]));
			
			data.push_back(sin((M_PI*2)*0.3201*static_cast<double>(i)+phases[1]));
			times.push_back((1.0/float(SAMPLES)) * (float(i)+phases[1]));
			
			data.push_back(sin((M_PI*2)*0.3201*static_cast<double>(i)+phases[2]));
			times.push_back((1.0/float(SAMPLES)) * (float(i)+phases[2]));
			
			data.push_back(sin((M_PI*2)*0.3201*static_cast<double>(i)+phases[3]));
			times.push_back( (1.0/float(SAMPLES)) * (float(i)+phases[3]));
	}
	std::map<std::string,std::string> keywords;
	keywords["marker"]=".";
	#ifdef PLOT
      plt::plot(times,data,keywords);
      plt::show();
    #endif
}